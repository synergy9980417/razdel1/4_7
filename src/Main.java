import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {
public static void main(String[] args) throws IOException {


//    Урок 7.
//    Обработка исключений, try.. catch
//    1.
//    В чем разница между throw и throws в Java?

    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//    throw - это оператор, который используется для генерации исключения в коде. Он позволяет явно выбросить исключение, если возникла ошибка или исключительная ситуация.
//throws - это ключевое слово, которое используется в сигнатуре метода для указания того, что метод может выбросить исключение. Оно информирует вызывающий код о том, что метод может генерировать исключение и что его нужно обработать.

//    2.
//    Пользователь вводит число. Если произошла ошибка ввода, выведите
//    пользователю сообщение о том, что ввод некорректный


    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

    Scanner scanner = new Scanner(System.in);
    try {
        System.out.println("Введите цифру:");
        char ch = scanner.next().charAt(0);
        if (!Character.isDigit(ch)) throw new RuntimeException("это не число");
    } catch (RuntimeException e) {
        System.err.println("поймали исключение " + e.getMessage());
        System.out.println(e.getStackTrace());
        e.printStackTrace();
    }

//    3.
//    Пользователь вводит число. Если произошла ошибка ввода, дайте
//    пользователю ввести еще раз: так пока он не введёт нормально (подсказка:
//    while)
    System.out.println("!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!!");
    int i3;
    while (true)
        try {
            System.out.println("Введите число:");
            i3 = scanner.nextInt();
            break;
        } catch (RuntimeException e) {
            System.err.println("поймали исключение, вы ввели не цифры " + e.getMessage());
            System.out.println(e.getStackTrace());
            e.printStackTrace();

            scanner.next(); // Очистка буфера ввода
        }
    System.out.println("Это нормально " + i3);


//    4.
//    Доработайте калькулятор. Если пользователь ввел неверное значение, просто
//    дайте ему ввести еще раз. (Пока он не вв
//    едёт корректно)
    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!");
    //доступно в этом ветке калькулятора
    //  https://gitlab.com/synergy9980417/tema1_urok1_5/-/blob/urok4_7/src/Main.java?ref_type=heads

//    5.
//    Пользователь вводит адрес ссылки. Если удаётся скачать страничку по адресу,
//    сохранить ее в html файл; иначе вывести, что страница не найдена.
    Scanner scanner1 = new Scanner(System.in);
    String str5 = "";
    String str5result = "";
    System.out.println("!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!!!!!!");
    System.out.println("Введите URL:");
    while (true) {
        try {
            str5 = scanner1.nextLine();
            str5result = getStr(str5);
            if (str5result.isEmpty()) throw new RuntimeException("не найдена страница по этому url = " + str5);
            break;
        } catch (RuntimeException e) {
            System.out.println("поймали исключение, вы ввели неверный адрес url " + e.getMessage());
//            System.out.println(e.getStackTrace());
//            e.printStackTrace();

            System.out.println("Введите URL:");
        }
    }

    writeToFile(str5result, "5.html");
    System.out.println("результат записан в 5.html");

//    6.
//    Пользователь вводит имя файла. Выведите содержимое этого файла, если не
//            получилось
//    -
//            выведите сооб
//    щение, что файл не найден

    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

    System.out.println("Введите имя файла:");
    while (true) {
        try {
            str5 = scanner1.nextLine();
            printFile(str5);
            break;
        } catch (RuntimeException e) {
            System.out.println("поймали исключение, вы ввели неверное имя файла " + e.getMessage());
            System.out.println("Введите имя файла:");
        }
    }


//    7.
//    Доработайте скачивальщик снимков NASA, чтобы в нем не было ни одного
//throws (пройдитесь прям поиском по файлу). Throws придётся заменить на try
//catch.


    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!! 7 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    String formatDate;
    LocalDate date;
    String page="";
    while (true)
    try {
        date = getLocalDateFromScanner();

        LocalDate startDate=LocalDate.of(1995,6,16);
        LocalDate endDate=LocalDate.now();
        //Date must be between Jun 16, 1995 and datenow
        if (date.isBefore(startDate)||date.isAfter(endDate)) throw new RuntimeException("Date must be between "+startDate+" и "+endDate);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        formatDate = date.format(formatter);
        System.out.println("На эту дату " + formatDate + " получим снимок ");
        String myIpKey = "b8BKYPTRSYvG6dKnwUbPAq8jht3kMxyrJ6WVBVRp";
// сразу все ссылки через get параметры api так быстрее, но задание про циклы
        page = downloadWebPage("https://api.nasa.gov/planetary/apod?api_key=" + myIpKey + "&start_date=" + formatDate + "&end_date=" + formatDate);
        if (page.equals("[]")) throw new RuntimeException("получили пустую json строку на эту дату");
        break;
    } catch (RuntimeException e){
        System.out.println("Видимо нужно ввести другую дату "+e.getMessage());
    }

   //парсим ссылку чтобы получить имя файла
    StringBuilder strb7 =new StringBuilder(page);
    String startStrUrl7="hdurl\":\"";
    String endStrUrl7="\",\"";
    String subUrlstr7=new String();
    int startUrl7;
    int endUrl7;
    try {
        //тут бывает что не изображение а видео поэтому другой формат парсинга. но если неудача пропускаем итерацию
        startUrl7 = strb7.indexOf(startStrUrl7) + startStrUrl7.length();
        endUrl7 = strb7.indexOf(endStrUrl7, startUrl7);
    } catch (Exception e){
        System.out.println("видимо это не изображение, изучите структуру ");
        System.out.println(page);
        System.out.println(e);
        return;
    };

    //сюда попадаем если не попадаем в catch значит у нас есть ссылка
    subUrlstr7=strb7.substring(startUrl7, endUrl7);
    //если не вышли из цикла сохраняем файл изображения
    //получаем имя файла из ссылки
    StringBuilder file=new StringBuilder();
    String[] arrFn=subUrlstr7.split("/");
    file.delete(0,file.length());
    file.append(arrFn[arrFn.length-1]);

    downloadImage(subUrlstr7,"files/"+formatDate+"__"+file.toString());


}

    private static String downloadWebPage(String url) throws IOException{
        StringBuilder result = new StringBuilder();
        String line;
        URLConnection urlConnection = new URL(url).openConnection();
        try (InputStream is = urlConnection.getInputStream();
             BufferedReader br= new BufferedReader(new InputStreamReader(is))){
            while ((line=br.readLine())!=null){
                result.append(line);
            }
        }
        return result.toString();



    }
//Метод скачивания изображения по url
    static void downloadImage(String url, String fullname) throws IOException{
        try(InputStream in = new URL(url).openStream()){
            Files.copy(in, Paths.get(fullname));
        } catch (Exception e) {System.out.println(e);}
    }



//    Критерии оценивания:
//    1 балл
//            -
//            создан новый проект в IDE
//    2 балла
//            -
//            написана обща
//    я структура программы
//    3 балла
//            -
//            выполнено более 60% заданий, имеется не более 5 критичных
//            замечаний
//    4 балла
//            -
//            выполнено корректно  более 80% технического задания, а также
//    продемонстрированы теоретические знания на удовлетворительном уровне
//    5 баллов
//            -
//            все
//    технические задания выполнены корректно, в полном объеме,
//    продемонстрированы  отличные теоретические знания при ответе на вопросы
//    Задание считается выполненным при условии, что слушатель получил оценку не
//    менее 3 баллов




    public static String getStr(String urlStr) {
        StringBuilder result = new StringBuilder();
        String line;
        try {
            URL url = new URL(urlStr);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            while ((line = reader.readLine()) != null) result.append(line);
            reader.close();
        } catch (Exception e) {
// ...
            System.out.println(e.getMessage());
        }

        return result.toString();
    }

    public static void writeToFile(String str, String fileName) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        writer.write(str);
        writer.close();
    }

    public static String getStringFromFile(String file, int numStr) throws FileNotFoundException {

        String path = new String();
        StringBuilder sb = new StringBuilder();
        String fullname = new String();
        String result ="";

        path = "files/";
        //из этого файла считываем имя первого файла. т.е. files/1
//        file="1";
        fullname=path+file;

        InputStream stream = new FileInputStream(fullname);

        Scanner filescan = new Scanner(stream);

        for (int i=1; i<=numStr;i++) {
            result=(filescan.nextLine());
        }
        return result;
    }



    public static void printFile(String file) throws FileNotFoundException {
        InputStream stream = new FileInputStream(file);
        Scanner filescan = new Scanner(stream);

        while (true)
        try{
            System.out.println(filescan.nextLine());
        }catch (Exception e){
         //   System.err.println("Дошли до конца файла");
            break;
        }


    }

    public static LocalDate getLocalDateFromScanner(){
        Scanner scanner = new Scanner(System.in);

        int[] prsDt=new int[3];
        System.out.println("Введите год YYYY:");
        prsDt[0]=scanner.nextInt();
        if (prsDt[0]<1992) throw new MySystemExceptions();

        System.out.println("Введите месяц:");
        prsDt[1]=scanner.nextInt();

        System.out.println("Введите день месяца:");
        prsDt[2]=scanner.nextInt();

        LocalDate rezult=LocalDate.of(prsDt[0],prsDt[1],prsDt[2]);
        System.out.println("вы ввели "+rezult);
        return rezult;
    }
// получаем формат строки 2022-01-01
    public static String getStrFromLocalDate(LocalDate ld)
    {
        StringBuilder sb=new StringBuilder();
        sb.insert(0,"-"+ld.getDayOfMonth());
        sb.insert(0,"-"+ld.getMonth());
        sb.insert(0,ld.getYear());

        return sb.toString();
    }

    public static class MySystemExceptions extends RuntimeException{
        MySystemExceptions(){
            super("исключение некорректный ввод. год не может быть менее чем 1992");
        }
    }
}
